package com.notes.pigeon_direct_messenger

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.notes.pigeon_direct_messenger.model.ChatChannel

class ChatAdapter(
    private var itens: MutableList<ChatChannel>,
    private val onItemClicked: ((ChatChannel) -> Unit)
): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ChatViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.channel_card, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder){
            is ChatViewHolder ->{
                holder.bind(itens[position], onItemClicked)
            }
        }
    }

    override fun getItemCount(): Int {
        return itens.size
    }

    class ChatViewHolder constructor(
        itemView: View

    ): RecyclerView.ViewHolder(itemView) {
        private val title = itemView.findViewById<TextView>(R.id.txt_title)
        private val preview = itemView.findViewById<TextView>(R.id.preview)

        fun bind(chat: ChatChannel, onItemClicked: (ChatChannel) -> Unit){
            title.text = chat.title
            preview.text = chat.lastMessage

            itemView.setOnClickListener{
                onItemClicked(chat)
            }

        }
    }

    fun createNewChat(title: String){
        itens.add(0, ChatChannel(title, ""))
    }

}