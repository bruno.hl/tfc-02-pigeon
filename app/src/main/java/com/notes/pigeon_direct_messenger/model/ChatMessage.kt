package com.notes.pigeon_direct_messenger.model

data class ChatMessage(
    val message: String,
    val isMessageSent: Boolean
    )
