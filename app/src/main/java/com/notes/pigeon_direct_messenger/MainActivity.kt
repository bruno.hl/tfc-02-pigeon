package com.notes.pigeon_direct_messenger

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import io.bloco.faker.Faker


class MainActivity : AppCompatActivity() {

    private lateinit var chatAdapter: ChatAdapter
    private lateinit var recyclerView: RecyclerView
    private lateinit var floatingButton: FloatingActionButton
    private lateinit var faker: Faker

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.floatingButton = findViewById(R.id.floatingButton)

        initRecyclerView()

        this.faker = Faker()

        this.floatingButton.setOnClickListener{
            newChat()
        }
    }

    private fun newChat() {
        val name: String = this.faker.name.firstName()
        this.chatAdapter.createNewChat(name)
        this.chatAdapter.notifyItemInserted(0)
        this.recyclerView.scrollToPosition(0)
    }

    private fun initRecyclerView() {
        this.chatAdapter = ChatAdapter(DataSource.createDataSet()){ chat ->
            val intent = Intent(this, MessageList::class.java)
            startActivity(intent)
        }

        this.recyclerView = findViewById(R.id.recyclerView)

        this.recyclerView.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = chatAdapter
        }
    }
}