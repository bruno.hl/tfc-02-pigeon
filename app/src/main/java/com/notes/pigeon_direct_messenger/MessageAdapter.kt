package com.notes.pigeon_direct_messenger

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.notes.pigeon_direct_messenger.model.ChatMessage

class MessageAdapter(
    private var itens: MutableList<ChatMessage>
): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MessageAdapter.MessageViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.message_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder){
            is MessageAdapter.MessageViewHolder ->{
                holder.bind(itens[position])
            }
        }
    }

    override fun getItemCount(): Int {
        return itens.size
    }

    class MessageViewHolder constructor(
        itemView: View
    ): RecyclerView.ViewHolder(itemView){
        private val sendMessage = itemView.findViewById<TextView>(R.id.send_message_box)
        private val receiveMessage = itemView.findViewById<TextView>(R.id.receive_message_box)

        fun bind(item: ChatMessage){
            sendMessage.text = item.message
            receiveMessage.text = item.message

            if (item.isMessageSent){
                receiveMessage.visibility = View.GONE
                sendMessage.visibility = View.VISIBLE
            }else{
                receiveMessage.visibility = View.VISIBLE
                sendMessage.visibility = View.GONE
            }
        }

    }

    fun insertMessage(message: ChatMessage){
        this.itens.add(message)
        notifyItemInserted(itens.size)
    }

}