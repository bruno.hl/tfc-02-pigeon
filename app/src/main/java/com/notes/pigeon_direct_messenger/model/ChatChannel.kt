package com.notes.pigeon_direct_messenger.model

data class ChatChannel(
    var title: String,
    var lastMessage: String
)
