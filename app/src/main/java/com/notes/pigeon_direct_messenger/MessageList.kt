package com.notes.pigeon_direct_messenger

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.notes.pigeon_direct_messenger.model.ChatMessage
import java.util.Random

class MessageList : AppCompatActivity() {

    private lateinit var messageAdapter: MessageAdapter
    private lateinit var recyclerView: RecyclerView
    private lateinit var floatingButton: FloatingActionButton
    private lateinit var input: EditText
    private lateinit var random: Random

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_message_list)

        this.input = findViewById(R.id.et_message)
        this.floatingButton = findViewById(R.id.floatingButton)
        this.random = Random()

        initRecyclerView()


        this.floatingButton.setOnClickListener{
            if(input.text.isNotBlank()) {
                val randNum = rand(1,3)
                when(randNum){
                    1 ->{
                        messageAdapter.insertMessage(ChatMessage(input.text.toString(), false))
                    }
                    2 ->{
                        messageAdapter.insertMessage(ChatMessage(input.text.toString(), true))
                    }
                }
                recyclerView.scrollToPosition(messageAdapter.itemCount-1)
                this.input.text.clear()
            }
        }

    }

    private fun initRecyclerView() {
        this.messageAdapter = MessageAdapter(MessageSource.createDataSet())

        this.recyclerView = findViewById(R.id.recyclerView)

        this.recyclerView.apply {
            layoutManager = LinearLayoutManager(this@MessageList)
            adapter = messageAdapter
        }
    }

    fun rand(from: Int, to: Int) : Int{
        return this.random.nextInt(to - from) + from
    }
}